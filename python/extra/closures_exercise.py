def counter():
    count=0
    def increment():
        nonlocal count
        count+=1
        return count
    return increment



a=counter()
# a=counter()
# # print(increment)
print(counter)
print(a)
print(a())
print(a())
print(a())


############################################
###calling function with its memory address#
############################################
def increment():
    count=0 
    count+=1
    return count

print(increment())
print(increment)
b=increment
print(b)
print(b())

##########################################################
###using other function without calling it(not actually)##
##########################################################
print("\n")
def increment2():
    count=0 
    count+=1
    print(count)

temp=increment2
print(temp)
        
def c():
    temp()

c()
print(increment2)
print(c)

###########
###globle##
###########

count=10
def counter():
    count=0
    def increment():
        global count
        count+=1
        return count
    return increment



a=counter()
print(a())
print(a())
print(a())
