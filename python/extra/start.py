

"""
     *
    **
   ***
  ****
 *****

"""

num = 5
for n in range(1, 6):   
    print(" " * num + "*" * n)
    num = num - 1

###########################################

for k in range(1,6):
    print("*" * k)

###########################################

#defining star method
def star(length):
    show=""
    for i in range(1,length):
        show += ("*" * i) + "\n"
    return show
    
print(star(20))

