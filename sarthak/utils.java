package sarthak;

import java.util.ArrayList;
import java.util.Random;

public class utils {

    public static String reverse(String input) {
        String reversedVar = "";
        for (int i = input.length(); i >= 1; i--) {
            reversedVar = reversedVar + input.charAt(i - 1);
        }
        return reversedVar.trim();
    }

    public static String randomString(int num) {
        Random random = new Random();
        String randomString = "";
        for (int i = 1; i <= num; i++) {
            char symbolStr = (char) random.nextInt(33, 122);
            randomString = randomString + symbolStr;
        }
        return randomString.trim();
    }

    public static String rmSpace(String input) {
        String checkChar = "";
        for (int i = 0; i <= input.length() - 1; i++) {
            if (input.charAt(i) == ' ') {
                checkChar = checkChar + "_";
            } else {
                checkChar = checkChar + input.charAt(i);
            }
        }
        return checkChar;
    }

    public static ArrayList<Integer> getArrayList(int length, int total) {
        ArrayList<Integer> fillArray = new ArrayList<Integer>();
        Random random = new Random();
        // how do i get smallest,biggest number by the length of num?
        int smallest = 0;
        int biggest = 0;
        for (int i = 0; i < total; i++) {
            fillArray.add(random.nextInt());
        }
        return fillArray;
    }

    public static int[] getArray(int length, int total) {
        int[] fillArray = new int[total];
        Random random = new Random();
        // how do i get smallest,biggest number by the length of num?
        int smallest = 0;
        int biggest = 0;
        for (int i = 0; i < total; i++) {
            fillArray[i] = random.nextInt();
        }
        return fillArray;
    }

}
