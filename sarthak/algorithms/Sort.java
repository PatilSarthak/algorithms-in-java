package sarthak.algorithms;

public class Sort {

    public static int[] bubbleSort(int[] listOfNums) {

        for (int check = 0; check <= listOfNums.length - 1; check++) {
            for (int i = 0; i <= listOfNums.length - 2; i++) {
                if (listOfNums[i] >= listOfNums[i + 1]) {
                    int temp = listOfNums[i];
                    listOfNums[i] = listOfNums[i + 1];
                    listOfNums[i + 1] = temp;
                }
            }
        }
        return listOfNums;
    }

    public static int[] selectionSort(int[] listOfNums) {
        // code is not efficient, need to remove redundant code

        for (int i = 0; i < listOfNums.length - 1; i++) {
            // Storing smallest value
            int temp = 0;
            // Storing the index of the smallest value
            int index = 0;

            if (listOfNums[i] >= listOfNums[i + 1]) {
                temp = listOfNums[i + 1];
                index = i + 1;

                for (int j = i + 2; j < listOfNums.length; j++) {
                    if (temp >= listOfNums[j]) {
                        temp = listOfNums[j];
                        index = j;
                    }

                }
                // swaping the values
                int temp2 = listOfNums[i];
                listOfNums[i] = temp;
                listOfNums[index] = temp2;

            }

            else {
                temp = listOfNums[i];
                index = i + 0;

                for (int j = i + 2; j < listOfNums.length; j++) {
                    if (temp >= listOfNums[j]) {
                        temp = listOfNums[j];
                        index = j;
                    }
                }
                int temp2 = listOfNums[i];
                listOfNums[i] = temp;
                listOfNums[index] = temp2;
            }
        }
        return listOfNums;
    }

    public static int[] inserationSort(int[] listOfNums) {

        // sorting an array forward
        for (int i = 0; i <= listOfNums.length - 2; i++) {
            if (listOfNums[i] >= listOfNums[i + 1]) {
                int temp = listOfNums[i];
                listOfNums[i] = listOfNums[i + 1];
                listOfNums[i + 1] = temp;
            }

            // sorting an array backword
            for (int j = i; j > 0; j--) {
                if (listOfNums[j] <= listOfNums[j - 1]) {
                    int temp = listOfNums[j];
                    listOfNums[j] = listOfNums[j - 1];
                    listOfNums[j - 1] = temp;
                }
            }
        }
        return listOfNums;
    }

}
