package sarthak.algorithms;

import java.util.ArrayList;

public class Search {

    public static int linearSearch(int[] listOfNums, int targetValue) {
        int index = 0;
        for (int i = 0; i <= listOfNums.length - 1; i++) {
            if (listOfNums[i] == targetValue) {
                index = i;
                break;
            }
        }
        return index;
    }

    // for returing multiple indices of values that has been repeated
    public static ArrayList<Integer> linearMultiSearch(int[] listOfNums, int targetValue) {
        ArrayList<Integer> indices = new ArrayList<Integer>();

        for (int i = 0; i <= listOfNums.length - 1; i++) {
            if (listOfNums[i] == targetValue) {
                indices.add(i);
            }
        }
        return indices;
    }

    public static int binarySearch(int[] listOfNums, int targetValue) {
        // Storing indices for calculating middle index of an array
        int first = 0;
        int last = listOfNums.length - 1;

        int mid = (first + last) / 2;
        while (listOfNums[mid] != targetValue) {

            if (listOfNums[mid] >= targetValue) {
                // changing the mid value to -1 because targetValue is smaller and need next mid
                // value
                last = mid - 1;
                mid = (last - first) / 2;
            }

            else {
                // changing the mid value to +1 because targetValue is bigger and need next mid
                // value
                first = mid + 1;
                mid = (last + first) / 2;
            }
        }
        return mid;
    }

    public static int ternarySearch(int[] listOfNums, int targetValue) {
        int first = 0;
        int last = listOfNums.length - 1;

        // array divided into 3 subarray
        int mid1 = first + (last - first) / 3;
        int mid2 = last - (last - first) / 3;
        int foundIt = 0;

        while (listOfNums[mid1] != targetValue && listOfNums[mid2] != targetValue) {
            // left sided subarray
            if (targetValue <= listOfNums[mid1]) {
                last = mid1 - 1;
                mid1 = first + (last - first) / 3;
                mid2 = last - (last - first) / 3;
            }
            // right sided subarray
            else if (targetValue >= listOfNums[mid2]) {
                first = mid2 + 1;
                mid1 = first + (last - first) / 3;
                mid2 = last - (last - first) / 3;
            }
            // middle subarray
            else {
                first = mid1 + 1;
                last = mid2 - 1;
                mid1 = first + (last - first) / 3;
                mid2 = last - (last - first) / 3;
            }
        }

        if (listOfNums[mid1] == targetValue) {
            foundIt = mid1;
        }

        else if (listOfNums[mid2] == targetValue) {
            foundIt = mid2;
        }
        return foundIt;
    }

}
